package com.pszumans.parking.exception;

public class ParkingMeterNotStoppedException extends ParkingException {
    public ParkingMeterNotStoppedException(String message) {
        super(message);
    }
}
