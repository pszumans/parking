package com.pszumans.parking.exception;

public class ParkingException extends RuntimeException {
    public ParkingException(String message) {
        super(message);
    }
}
