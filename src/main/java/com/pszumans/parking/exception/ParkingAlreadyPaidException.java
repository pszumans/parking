package com.pszumans.parking.exception;

public class ParkingAlreadyPaidException extends ParkingException {
    public ParkingAlreadyPaidException(String message) {
        super(message);
    }
}
