package com.pszumans.parking.exception;

public class NotPaidParkingException extends ParkingException {
    public NotPaidParkingException(String message) {
        super(message);
    }
}
