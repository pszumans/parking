package com.pszumans.parking.exception;

public class VehicleUnregisteredException extends ParkingException {
    public VehicleUnregisteredException(String message) {
        super(message);
    }
}
