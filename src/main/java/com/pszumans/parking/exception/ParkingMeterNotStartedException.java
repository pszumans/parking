package com.pszumans.parking.exception;

public class ParkingMeterNotStartedException extends ParkingException {
    public ParkingMeterNotStartedException(String message) {
        super(message);
    }
}
