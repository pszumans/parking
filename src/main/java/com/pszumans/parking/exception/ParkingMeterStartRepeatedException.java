package com.pszumans.parking.exception;

public class ParkingMeterStartRepeatedException extends ParkingException {

    public ParkingMeterStartRepeatedException(String message) {
        super(message);
    }
}
