package com.pszumans.parking.exception;

public class NotAvailableCurrencyException extends ParkingException {
    public NotAvailableCurrencyException(String message) {
        super(message);
    }
}
